<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::auth();
Route::group(["middleware" => "auth"], function(){
	Route::get('/', 'HomeController@index')->name('home');
	Route::group(["prefix" => "email"], function(){
		Route::group(["prefix" => "compose"], function(){
			Route::get('/', 'EmailController@compose')->name('compose-email');
			Route::post('/add/{id?}', 'EmailController@createEmail')->name('compose-create');
		});
		Route::get('/sentbox', "EmailController@sentbox")->name('sentbox');
		Route::get('/inbox', "EmailController@inbox")->name('inbox');
		Route::get('/detail/{id}', "EmailController@detail")->name('read');

		Route::group(["prefix" => "draft"], function(){
			Route::get('/', "EmailController@draft")->name('draft');
		});
	});
});
Route::get('/login', "HomeController@login")->name('login');
Route::get('/logout', 'HomeController@logout')->name('logout');
