<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailReceiver extends Model
{
    protected $table = "receiver";
    protected $fillable = ["user_id", "inbox_id", "user_type", "status"];

    public function receivers(){
    	return $this->belongsTo('App\EmailModel', "id");
    }
}
