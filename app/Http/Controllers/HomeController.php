<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $api_url = "";
    public function __construct()
    {
        $this->api_url = env('API_URL').'/api/';
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return redirect()->route('inbox');
    }

    public function logout()
    {
        session()->flush();
        $url = env('API_URL');
        return redirect()->to($url.'logout?continue='.url("/"));
    }

    public function login()
    {
        $url = env('API_URL');
        return redirect()->to($url.'login?continue='.url('email/compose'));
    }
}
