<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmailModel;
use App\EmailReceiver;
use Carbon\Carbon;

class EmailController extends Controller
{
    //
    protected $api_url = "";
    protected $page = "email";
    function __construct()
    {
    	$this->api_url = env('API_URL').'api/';
    }

    public function compose(Request $request)
    {
    	$headers = [
    		"Accept: application/json"
    	];
        if (!empty($request->get('id'))) {
            $content = EmailReceiver::with('receivers')->join('inbox', 'inbox.id', '=', 'receiver.inbox_id')->where('receiver.status', '=', 3)->where('inbox_id', '=', $request->get('id'))->first();
            $receiver = EmailReceiver::where('inbox_id', '=', $content->id)->get();
            $user_list = doCurl($this->api_url.'user/request-data?with_email=true&email='.session('email'), $headers);

            $data['users'] = [];
            $data['contents'] = $content;
            $data['receivers'] = $receiver;
            $data['total_inbox'] = $this->getTotalInbox(session('user_id'));
            $data['inbox_id'] = $request->get('id');
            if ($user_list->code == 200) {
                $data['users'] = $user_list->data;
            }
            $data['total_inbox'] = $this->getTotalInbox(session('user_id'));
            return view('dashboard.compose', $data);
        }
    	$user_list = doCurl($this->api_url.'user/request-data?with_email=true&email='.session('email'), $headers);
    	$users = [];
    	$data['users'] = $users;
        $data['total_inbox'] = $this->getTotalInbox(session('user_id'));
    	if ($user_list->code == 200) {
    		$data['users'] = $user_list->data;
    	}
    	return view('dashboard.compose', $data);
    }

    public function sentbox()
    {
        $email = new EmailReceiver;
        $emails = $email->with('receivers')->join('inbox', 'inbox.id', '=', 'receiver.inbox_id')->where('receiver.status', '<', 3)->get();
        $this->page = "sentbox";
        $data['page'] = $this->page;
        $data['emails'] = $emails;
        $data['total_inbox'] = $this->getTotalInbox(session('user_id'));

    	return view('dashboard.sentbox', $data);
    }

    public function inbox(){
        $email = new EmailReceiver;
        $emails = $email->with('receivers')->join('inbox', 'inbox.id', '=', 'receiver.inbox_id')->where('receiver.user_id', '=', session("user_id"))->get();
        $this->page = "inbox";
        $data['page'] = $this->page;
        $data['emails'] = $emails;
        $data['total_inbox'] = $this->getTotalInbox(session('user_id'));

        return view('dashboard.inbox', $data);
    }

    public function detail($id_email)
    {
        $emails = EmailModel::where('id', '=', $id_email)->get();
        $receiver = EmailReceiver::where('inbox_id', '=', $id_email)->where('user_id', '=', session('user_id'))->first();
        $receiver->status = 2;
        $receiver->save();

        $this->page = "inbox";
        $data['page'] = $this->page;
        $data['emails'] = $emails;
        $data['total_inbox'] = $this->getTotalInbox(session('user_id'));

        return view('dashboard.email_read', $data);
    }

    public function createEmail(Request $request)
    {
        $headers = [
            "Accept: application/json"
        ];
        if (!empty($request->input('inbox_id'))) {
            $emails = EmailReceiver::where('inbox_id', '=', $request->input('inbox_id'))->get();
            if (!empty($emails)) {
                foreach ($emails as $email) {
                    $email->status = 2;
                    $email->save();
                }
                return redirect()->route('sentbox');
            }
        }

    	$receivers = $request->input('receiver');

        $inbox = new EmailModel;
        $inbox->sender_id = session('user_id');
        $inbox->sender_email = session('email');
        $inbox->sender_name = session('user_name');
        $inbox->subject = $request->input('subject');
        $inbox->body = $request->input('body');
        $inbox->sended_at = Carbon::now();
        // $inbox->save();
        $inbox_id = $inbox->id;

        foreach ($receivers as $receiver) {
            $data = doCurl($this->api_url.'user/request-data?with_id=true&id='.$receiver, $headers);

    		$emailReceiver = new EmailReceiver;
    		$emailReceiver->user_id = $receiver;
            $emailReceiver->user_email = $data->data->email;
            $emailReceiver->user_type = "receiver";
            if ($request->input('type') == 1) {
                $emailReceiver->status = 2;
            }else{
                $emailReceiver->status = 3;
            }
            $emailReceiver->inbox_id = $inbox_id;
    		// $emailReceiver->save();
    	}
        return redirect()->route('sentbox');
    }

    public function draft()
    {
        $this->page = "draft";
        $data['page'] = $this->page;
        $data['total_inbox'] = $this->getTotalInbox(session('user_id'));

        $emails = EmailReceiver::with('receivers')->join('inbox', 'inbox.id', '=', 'receiver.inbox_id')->where('receiver.status', '=', 3)->groupBy('inbox_id')->get();
        // dd(count($emails) > 0);
        if (count($emails) > 0) {
            $receiver = EmailReceiver::where('inbox_id', '=', $emails->first()->id)->get();
            $data['emails'] = $emails;
            $data['receivers'] = $receiver;
            return view('dashboard.draft', $data);
        }else{
            $data['emails'] = [];
            $data['receivers'] = [];
            return view('dashboard.draft', $data);
        }
    }

    public function getTotalInbox($user_id)
    {
        $email = new EmailReceiver;
        $emails = $email->with('receivers')->join('inbox', 'inbox.id', '=', 'receiver.inbox_id')->where('receiver.user_id', '=', $user_id)->where('status', '=', 1)->count();
        return $emails;
    }

}
