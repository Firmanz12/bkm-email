<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EmailReceiver;

class EmailModel extends Model
{
    //
    protected $table = 'inbox';

   public function sentboxes(){
   	return $this->hasMany('App\EmailReceiver', 'inbox_id');
   }

   public static function getSentBox($user_id, $user_type = "sender")
   {
   		if (!empty($user_id)) {
   			$query = static::select('inbox.subject', 'inbox.sender_id', 'inbox.body', 'receiver.user_id', 'receiver.inbox_id', 'receiver.user_type')->join('receiver', 'receiver.inbox_id', "=", 'inbox.id')->where('inbox.sender_id', '=', $user_id);
        	return $query;
   		}
   }
}
