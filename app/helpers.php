<?php 
  function doCurl($url="", $headers=array()) {
    $ch = curl_init($url);
    curl_setopt_array($ch, array(
        CURLOPT_HTTPGET  => 1,
        CURLOPT_HTTPHEADER  => $headers,
        CURLOPT_RETURNTRANSFER  =>true,
        CURLOPT_VERBOSE     => 1
    ));
    $response = curl_exec($ch);
    $response = json_decode($response);
    curl_close($ch);
    return $response;
  }