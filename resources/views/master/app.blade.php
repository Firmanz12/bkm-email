<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>BKM EMAIL</title>

    <!-- Styles -->
    <link href="{{ asset('dashboard/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->

    <link href="{{ asset('dashboard/css/lib/calendar2/semantic.ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/lib/calendar2/pignose.calendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/select2/css/select2.css') }}" rel="stylesheet">

</head>
</head>
<body class="fix-header fix-sidebar">
    <div class="header">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- Logo -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <!-- Logo icon -->
                    <b><img src="{{ asset('image/bkm-logo.png') }}" alt="homepage" class="dark-logo" style="width: 100%;padding: 15px" /></b>
                    <!--End Logo icon -->
                </a>
            </div>
            <!-- End Logo -->
        </nav>
    </div>
    <div class="left-sidebar">
           <!-- Sidebar scroll-->
           <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <div class="card" style="box-shadow: none;">
                    <div class="row">
                        <div class="col-md-12" style="padding-right: 0px!important;padding-left: 0px!important">
                            <div class="card-body">
                                <div class="card-content">
                                    <div class="inbox-leftbar">
                                        <a class="btn btn-danger btn-block waves-effect waves-light" href="{{ route('compose-email') }}">Compose</a>
                                        <div class="mail-list mt-4">
                                            <a class="list-group-item border-0 <?php if(!empty($page) && $page == "inbox"){ echo "text-danger";}else{ echo "";} ?>" href="{{ route('inbox') }}"><i class="mdi mdi-inbox font-18 align-middle mr-2"></i><b>Inbox</b><span class="label label-danger float-right ml-2">
                                                <?php if (!empty($total_inbox)) {
                                                    echo $total_inbox;
                                                }else{
                                                    echo "0";
                                                } ?>
                                            </span></a>
                                            <a class="list-group-item border-0 <?php if(!empty($page) && $page == "draft"){ echo "text-danger";}else{ echo "";} ?>" href="{{ route('draft') }}"><i class="mdi mdi-file-document-box font-18 align-middle mr-2"></i>Draft<span class="label label-info float-right ml-2">0</span></a>
                                            <a class="list-group-item border-0 <?php if(!empty($page) && $page == "sentbox"){ echo "text-danger";}else{ echo "";} ?>" href="{{ route('sentbox') }}"><i class="mdi mdi-send font-18 align-middle mr-2"></i>Sent Mail</a>
                                            <a class="list-group-item border-0" href="#"><i class="mdi mdi-delete font-18 align-middle mr-2"></i>Trash</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
               <!-- End Sidebar navigation -->
           </div>
           <!-- End Sidebar scroll-->
       </div>
    @yield('content')
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="{{ asset('dashboard/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('dashboard/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('dashboard/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('dashboard/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('dashboard/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('dashboard/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->


    <!-- Amchart -->
     <script src="{{ asset('dashboard/js/lib/morris-chart/raphael-min.js') }}"></script>
    <script src="{{ asset('dashboard/js/lib/morris-chart/morris.js') }}"></script>
    <script src="{{ asset('dashboard/js/lib/morris-chart/dashboard1-init.js') }}"></script>


    <script src="{{ asset('dashboard/js/lib/calendar-2/moment.latest.min.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ asset('dashboard/js/lib/calendar-2/semantic.ui.min.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ asset('dashboard/js/lib/calendar-2/prism.min.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ asset('dashboard/js/lib/calendar-2/pignose.calendar.min.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ asset('dashboard/js/lib/calendar-2/pignose.init.js') }}"></script>

    <script src="{{ asset('dashboard/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('dashboard/js/lib/owl-carousel/owl.carousel-init.js') }}"></script>
    <script src="{{ asset('dashboard/js/scripts.js') }}"></script>
    <!-- scripit init-->

    <script src="{{ asset('dashboard/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('dashboard/js/scripts.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.receiver-selector').select2();
        });
    </script>

</body>
</html>