@extends('master.app')

@section('content')
<div class="main-wrapper">
	<div class="page-wrapper">
		<div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-primary">Compose</h3>
            </div>
        </div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="card-content">
								<div class="mt-4">
								    <form action="{{ route('compose-create') }}" method="POST">
								    	{{ csrf_field() }}
								        <div class="form-group">
								        	<select class="receiver-selector form-control" name="receiver[]" multiple="true">
								        		<?php foreach ($users as $user): ?>
								        			<option value="{{ $user->id }}">{{ $user->email }}</option>
								        		<?php endforeach ?>
								        	</select>
								        </div>
								        <div class="form-group">
								            <input type="text" class="form-control" placeholder="Subject" name="subject">
								        </div>
								        <div class="form-group">
								            <textarea name="body" rows="8" cols="80" class="form-control" style="height:300px"></textarea>
								        </div>
								        <div class="form-group m-b-0">
								            <div class="text-right">
								                <button type="submit" value="2" class="btn btn-success waves-effect waves-light m-r-5"><i class="fa fa-floppy-o"></i></button>
								                <button type="submit" value="1" class="btn btn-success waves-effect waves-light m-r-5"><i class="fa fa-send"></i></button>
								            </div>
								        </div>
								    </form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection