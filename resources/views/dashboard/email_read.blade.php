@extends('master.app')

@section('content')
<div class="preloader">
	<svg class="circular" viewBox="25 25 50 50">
		<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
	</svg>
</div>
<div id="main-wrapper">
	<div class="page-wrapper">
		<div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-primary">Email</h3>
            </div>
        </div>
		<div class="container-fluid">
		<!-- Start Page Content -->
			<div class="row">
			    <div class="col-12">
			        <div class="card">
			            <div class="card-body">
			                <div class="card-content">
			                    <!-- Left sidebar -->
			                    <!-- End Left sidebar -->
			                    <div class="col-12">
			                        <?php foreach ($emails as $email): ?>
			                        	<div class="">
			                        	    <div class="mt-4">
                                    	        <h5>{{ $email->subject }}</h5>
	
                                    	        <hr/>
	
                                    	        <div class="media mb-4 mt-1">
                                    	            <div class="media-body">
                                    	                <span class="pull-right">{{ date("H:i A", strtotime($email->sended_at)) }}</span>
                                    	                <h6 class="m-0">{{ $email->sender_name }}</h6>
                                    	                <small class="text-muted">From: {{ $email->sender_email }}</small>
                                    	            </div>
                                    	        </div>
	
                                    	        <p>{{ $email->body }}</p>
	
                                    	        <hr/>
                                    	    </div>
			                        	    <!-- panel body -->
			                        	</div>
			                        <?php endforeach ?>
			                    </div>
			                    <div class="clearfix"></div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		    <!-- End PAge Content -->
		</div>
	</div>
</div>
@endsection