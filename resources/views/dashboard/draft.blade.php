@extends('master.app')

@section('content')
<div class="preloader">
	<svg class="circular" viewBox="25 25 50 50">
		<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
	</svg>
</div>
<div id="main-wrapper">
	<div class="page-wrapper">
		<div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-primary">Draft</h3>
            </div>
        </div>
		<div class="container-fluid">
		<!-- Start Page Content -->
			<div class="row">
			    <div class="col-12">
			        <div class="card">
			            <div class="card-body">
			                <div class="card-content">
			                    <!-- Left sidebar -->
			                    <!-- End Left sidebar -->
			                    <div class="col-12">
			                        <div class="">
			                            <div class="mt-4">
			                                <div class="">
			                                    <ul class="message-list">
			                                        <?php if (!empty($emails)): ?>
			                                        	<?php foreach ($emails as $email): ?>
			                                        		<li class="unread">
                                                    		    <a href="{{ route('compose-email', ['id' => $email->inbox_id]) }}">
                                                    		        <div class="col-mail col-mail-1">
                                                    		            <p class="title">
                                                    		            	<?php foreach ($receivers as $receiver): ?>
                                                    		            		{{ $receiver->user_email }},
                                                    		            	<?php endforeach ?>
                                                    		            </p>
                                                    		        </div>
                                                    		        <div class="col-mail col-mail-2">
                                                    		            <div class="subject">
                                                    		            	{{ $emails->subject }}
                                                    		            </div>
                                                    		            <div class="date">{{ date('H:i A', strtotime($emails->sended_at)) }}</div>
                                                    		        </div>
                                                    		    </a>
                                                    		</li>
			                                        	<?php endforeach ?>
			                                        <?php endif ?>
			                                    </ul>
			                                </div>
			                            </div>
			                            <!-- panel body -->
			                        </div>
			                        <!-- panel -->
			                        <div class="row">
			                            <div class="col-7">
			                                Showing 1 - 20 of 2
			                            </div>
			                            <div class="col-5">
			                                <div class="btn-group float-right">
			                                    <button class="btn btn-gradient waves-effect" type="button"><i class="fa fa-chevron-left"></i></button>
			                                    <button class="btn btn-gradient waves-effect" type="button"><i class="fa fa-chevron-right"></i></button>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                    <div class="clearfix"></div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		    <!-- End PAge Content -->
		</div>
	</div>
</div>
@endsection